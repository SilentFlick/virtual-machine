# virtual-machine

A guide for virtual machines with GPU passthrough that can be used over the internet using tailscale, QEMU and libvirt.

To access the VM over the internet I use Tailscale, but you can also use wireguard or OpenVPN.

## Requirements
+ QEMU
+ libvirt
+ iptables-nft
+ dnsmasq
+ dmidecode
+ virt-manager

Note: the package name may be different from distro to distro.

## Installation
1. Add user to group libvirt: `usermod -aG libvirt silentflick`
2. Enable libvirtd.service: `systemctl enable --now libvirtd`

If somehow your VM does not have an IP address, you may need to edit /etc/dnsmasq.conf

```
#port=5353
interface=eth0
bind-interfaces
```

3. Enable IOMMU for GPU passthrough
Edit: 
    + append `intel_iommu=on iommu=pt` to GRUB_CMDLINE_LINUX_DEFAULT in /etc/defauflt/grub. For AMD CPU, it is on if kernl detects IOMMU hardware support from BIOS. Remember to reboot first.
    + add file /etc/modprobe.d/nvidia.conf
    ```
    softdep nouveau pre: vfio-pci
    softdep nvidia pre: vfio-pci
    softdep nvidia* pre: vfio-pci
    ```
    + add file /etc/modprobe.d/vfio.conf
    ```
    options vfio-pci ids=10de:2489,10de:228b
    ```
    I obtain `10de:2489,10de:228b` via [this script](https://wiki.archlinux.org/title/PCI_passthrough_via_OVMF#Ensuring_that_the_groups_are_valid).
    + add file /etc/modprobe.d/kvm.conf
    ```
    options kvm ignore_msrs=1
    ```
4. Regenerate the initramfs
5. Create VM in virt-manager, add PCI Host Device before installation


## References
+ https://wiki.archlinux.org/title/PCI_passthrough_via_OVMF
+ https://doc.opensuse.org/documentation/leap/archive/42.3/virtualization/html/book.virt/cha.libvirt.networks.html
+ https://github.com/pavolelsig/passthrough_helper_opensuse/blob/master/gpu_passthrough.sh
+ https://yewtu.be/watch?v=Nu2bHV8mA6c
+ https://mathiashueber.com/performance-tweaks-gaming-on-virtual-machines/

## TODO
+ Improve the latency
+ When playing video in VM, there are glitches
